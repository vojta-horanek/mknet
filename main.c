#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define error(msg) fprintf(stderr, "ERROR: %s\n", (msg))
#define info(msg) fprintf(stderr, "%s\n", (msg))

#define print_ip(ip)                                                           \
  printf("%d.%d.%d.%d", ip.octet[3], ip.octet[2], ip.octet[1], ip.octet[0]);

#define SP_VER '|'
#define SP_HOR '-'
#define SP_HOR_FULL "---------------"

typedef union ip_addr {
  unsigned char octet[4];
  int addr;
} IP_ADDR;

typedef unsigned char byte;
typedef char *string;

#define IP_STR_MAX 15

int sort_decreasing(const void *a, const void *b) {
  int *x = (int *)a;
  int *y = (int *)b;
  return *y - *x;
}

byte string_to_byte(const byte *str) {
  byte result = 0;
  for (int i = 0; str[i] != 0; i++) {
    result = result * 10 + str[i] - '0';
  }
  return result;
}

void ip_to_string(IP_ADDR ip, char out[IP_STR_MAX + 1]) {
  snprintf(out, IP_STR_MAX + 1, "%d.%d.%d.%d", ip.octet[3], ip.octet[2],
           ip.octet[1], ip.octet[0]);
}

IP_ADDR string_to_ip(string ip) {
  IP_ADDR result;
  result.addr = 0;
  int addr_pos = 3;

  // I could use a custom atoi that only parses n chars instead of appending an
  // additional character for null
  byte octet[4];
  int oct_pos = 0;

  for (int i = 0; ip[i] != 0; i++) {
    if (ip[i] == '.' || ip[i + 1] == 0) {
      // read the last character, could probably be done cleaner
      if (ip[i + 1] == 0)
        octet[oct_pos++] = ip[i];
      octet[oct_pos] = 0;
      result.octet[addr_pos--] = string_to_byte(octet);
      oct_pos = 0;
      continue;
    }
    octet[oct_pos++] = ip[i];
  }

  return result;
}

void print_cell(char *buffer, int minChar) {
  putchar(SP_VER);
  putchar(' ');

  int cellLength = strlen(buffer);
  for (int i = 0; i < minChar; i++) {
    if (i < cellLength)
      putchar(buffer[i]);
    else
      putchar(' ');
  }
  putchar(' ');
}

IP_ADDR make_network(IP_ADDR ip, int basePrefix, int clients, int isLimit,
                     int limit) {
  IP_ADDR final_address;
  final_address.addr = 0;
  // determine which prefix should a single subnet use
  int prefix = 32;
  while (pow(2, 32 - prefix) < clients + 2)
    prefix--;
  if (prefix < basePrefix) {
    error("!!---- Too many clients in a network that is too small.");
    return final_address;
  }

  // determine how many subnets are with the dermined prefix are possible in the
  // network
  int numOfSubnets = pow(2, prefix - basePrefix);

  const int clientsInOneSubnet = pow(2, 32 - prefix);

  char buffer[IP_STR_MAX];

  for (int i = 0; i < numOfSubnets; i++) {

    if (isLimit == 1 && i >= limit)
      break;

    IP_ADDR netIP;
    netIP.addr = ip.addr + i * clientsInOneSubnet;

    IP_ADDR brdIP;
    brdIP.addr = netIP.addr + clientsInOneSubnet - 1;
    final_address.addr = brdIP.addr + 1;

    IP_ADDR firstIP;
    firstIP.addr = netIP.addr + 1;

    IP_ADDR lastIP;
    lastIP.addr = brdIP.addr - 1;

    IP_ADDR ips[4] = {netIP, brdIP, firstIP, lastIP};

    for (int i = 0; i < 4; i++) {
      ip_to_string(ips[i], buffer);
      print_cell(buffer, IP_STR_MAX - 1);
    }

    snprintf(buffer, 3, "%d", prefix);
    print_cell(buffer, 6);

    putchar(SP_VER);
    putchar('\n');
  }
  return final_address;
}

void print_header(void) {
  print_cell("Network", IP_STR_MAX - 1);
  print_cell("Broadcast", IP_STR_MAX - 1);
  print_cell("First client", IP_STR_MAX - 1);
  print_cell("Last client", IP_STR_MAX - 1);
  print_cell("Prefix", 6);

  putchar(SP_VER);
  putchar('\n');

  print_cell(SP_HOR_FULL, IP_STR_MAX - 1);
  print_cell(SP_HOR_FULL, IP_STR_MAX - 1);
  print_cell(SP_HOR_FULL, IP_STR_MAX - 1);
  print_cell(SP_HOR_FULL, IP_STR_MAX - 1);
  print_cell(SP_HOR_FULL, 6);

  putchar(SP_VER);
  putchar('\n');
}

IP_ADDR parse_ip(char **argv, int *prefix) {
  char buffer[IP_STR_MAX + 1];
  int i;
  for (i = 0; argv[2][i] != '/' && i < IP_STR_MAX + 1; i++) {
    buffer[i] = argv[2][i];
  }
  buffer[i++] = 0;
  *prefix = atoi(argv[2] + i * sizeof(char));
  return string_to_ip(buffer);
}

void main_flsm(int argc, char **argv) {
  int isLimit = 0;
  int limit = 0;

  if (argc == 5) {
    isLimit = 1;
    limit = atoi(argv[4]);
    if (limit <= 0)
      exit(0);
  }
  int prefix = 0;
  IP_ADDR network = parse_ip(argv, &prefix);
  print_header();
  make_network(network, prefix, atoi(argv[3]), isLimit, limit);
}

void main_vlsm(int argc, char **argv) {
  char buffer[IP_STR_MAX + 1];
  int i;
  for (i = 0; argv[2][i] != '/'; i++) {
    buffer[i] = argv[2][i];
  }
  buffer[i++] = 0;
  int prefix = 0;
  IP_ADDR network = parse_ip(argv, &prefix);

  /* Map the argument array to int and sort it from sort_decreasing order */
  const int clients_len = argc - 3;
  int *clients = malloc(sizeof(int) * clients_len);

  for (int i = 3; i < argc; i++) {
    clients[i - 3] = atoi(argv[i]);
  }

  qsort(clients, clients_len, sizeof(int), sort_decreasing);

  print_header();
  for (int i = 0; i < clients_len; i++) {
    network = make_network(network, prefix, clients[i], 1, 1);
  }

  free(clients);
}

int main(int argc, string *argv) {
  if (argc < 3) {
    info("Usage: make_network lfsm <addres>/<prefix> <clients in one subnet> "
         "[num of subnets]");
    info("       make_network vlsm <addres>/<prefix> [subnet one clients, "
         "subnet two clients...]");
    exit(1);
  }

  if (strncmp(argv[1], "flsm", 4) == 0) {
    if (argc < 4) {
      error("Specify number of clients in one subnet!");
      exit(1);
    }
    main_flsm(argc, argv);
  } else if (strncmp(argv[1], "vlsm", 4) == 0) {
    if (argc < 4) {
      error("Specify at least one subnet!");
      exit(1);
    }
    main_vlsm(argc, argv);
  } else {
    error("Unknown mode, use `flsm` or `vlsm`");
    exit(1);
  }

  return 0;
}
